import { EmployeeService } from './../employee.service';
import { Employee } from './../employee';
import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {
  employee: Employee = new Employee();
  constructor(private employeeService: EmployeeService, private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.saveEmployee();
  }

  goToEmployeeList() {
    this.router.navigate(['/employees']);
  }

  saveEmployee() {
    this.employeeService.createEmployee(this.employee)
      .subscribe((response: Employee) => {
        console.log(response);
        this.goToEmployeeList();
      }
        , (error: HttpErrorResponse) => console.log(error)
      );
  }

}
