import { Router } from '@angular/router';
import { EmployeeService } from './../employee.service';
import { Component, OnInit } from '@angular/core';
import { Employee } from "../employee";

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  public employees: Employee[] = [];
  constructor(private employeeService: EmployeeService, private router: Router) { }

  ngOnInit(): void {
    this.getEmployees();
  }

  getEmployees() {
    this.employeeService.getEmployeesList()
      .subscribe((response: Employee[]) => this.employees = response);

  }

  updateEmployee(id: number | undefined) {
    this.router.navigate(['update-employee', id]);
  }

  deleteEmployee(id: number | undefined) {
    this.employeeService.deleteEmployee(id).subscribe(response => {
      this.getEmployees();
    })
  }

}
